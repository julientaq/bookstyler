const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const cleanWebPackPlugin = require("clean-webpack-plugin");
const ManifestPlugin = require("webpack-manifest-plugin");
const postcssLoader = require('postcss-loader');
const fileLoader = require('file-loader');
const imgLoader = require('img-loader');
const urlLoader = require('url-loader');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const WriteFilePlugin = require('write-file-webpack-plugin');
const CssUrlRelativePlugin = require('css-url-relative-plugin')
const path = require('path');


module.exports = {
    entry: {
        // bookStyle: './src/bookStyle.js',
        pagedMedia: './src/paged-media_layout.js',
        vivliostyle: './src/vivliostyle.js',
        editoria: './src/editoria-default.js'
    },
    output: {
        path: path.resolve('./dist'),
        filename: 'js/[name].js'
    },
    devServer: {
        port: 9012
    },
    devtool: "source-map",
    module: {
        rules: [{
                // pass all js through babel
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },

            //if sass comes back in the game on day.

            // {
            //     test: /\.scss$/,
            //     use: [
            //         //  'style-loader', 

            //         MiniCssExtractPlugin.loader,
            //         'css-loader',
            //         {
            //             loader: 'postcss-loader',
            //             options: {
            //                 sourceMap: true,
            //                 plugins: (loader) => [
            //                     require('postcss-import'),
            //                     require('postcss-nested'),
            //                     require('postcss-css-variables')
            //                 ]
            //             }
            //         },
            //         'resolve-url-loader',
            //         'sass-loader'
            //     ]
            // },
            {
                test: /\.css$/,
                use: [
                    //  'style-loader', 
                    MiniCssExtractPlugin.loader,
                    { loader: 'css-loader', options: { sourceMap: true } },
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: true ,
                            plugins: (loader) => [
                                require('postcss-import'),
                                require('postcss-mixins'),
                                require('postcss-nested'),
                                require('postcss-css-variables')
                            ]
                        }
                    },
                    'resolve-url-loader'

                ]

            },
            // pugs specs-file to html
            {
                test: /\.pug$/,
                use: ['file-loader?name=[name].html', 'pug-html-loader']
            },
            // fonts
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
                loader: 'file-loader?name=assets/fonts/[name].[ext]'
            },
            //   images
            {
                test: /\.(png|jpg|gif|svg)$/,
                use: [{
                        loader: 'url-loader',
                        options: {
                            limit: 8192,
                            name: '[path][name].[ext]'
                        }
                    },
                    {
                        loader: 'img-loader?name=assets/img/[name].[ext]',
                        options: {
                            enabled: false
                        }
                    }
                ]
            }

        ]
    },
    plugins: [
        // delete dist folder 
        // new cleanWebPackPlugin(['dist']),
        new MiniCssExtractPlugin({
            filename: './assets/[name].css',
        }),
        // new HtmlWebpackPlugin({
        //     inject: false,
        //     hash: true,
        //     template: './src/index.html',
        //     filename: 'index.html'
        // }),
        new CssUrlRelativePlugin(/* options */), //to keep the url as relative in css files BINGO! ;D
        new ManifestPlugin(),
        new WriteFilePlugin()
    ]
}